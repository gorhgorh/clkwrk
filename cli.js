#!/usr/bin/env node
const path = require('path')
const debug = require('debug')('clkwrk:cli')
const parsers = require(path.join(__dirname, './tools/cmdParser'))
const { cmdParser, cmdSelector } = parsers

const adaptTools = require('./tools/adaptTools')

const { updatePlp } = adaptTools

function cli (args) {
  debug('cli:init', args)
  const cmds = cmdSelector(cmdParser(args))
  debug(cmds)
  if (cmds === false) {
    console.log('no valid command')
    return
  }
  // console.log(cmds)
  cmds.forEach(cmd => {
    console.log(cmd)
    switch (cmd.name) {
      case 'addPlp':
        console.log('PLP', cmd)
        updatePlp(path.join(process.cwd(), cmd.val), 'text')
        break
      default:
        console.log('cmd not found', cmd.name)
    }
  })
  return cmds
}

cli(process.argv)
