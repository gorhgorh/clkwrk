module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true
  },
  extends: [
    'standard'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    expect: true,
    test: true,
    describe: true,
    it: true,
    beforeAll: true
  },
  parserOptions: {
    ecmaVersion: 2018
  },
  rules: {
  }
}
