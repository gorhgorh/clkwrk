'use strict'
const cmdName = 'zip'
const cmdMsg = 'zip dirs'
const debug = require('debug')('clkwrk:' + cmdName)
const zipDir = require('../tools/adapt/createArchive')

const zipCmd = function () {
  debug('starting zip cmd')

}

module.exports = {
  cmd: zipCmd,
  cmdName,
  cmdMsg
}
