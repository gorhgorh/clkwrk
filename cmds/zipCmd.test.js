const zipCmd = require('./zipCmd')
const path = require('path')
const destP = path.join(__dirname, '../fixtures/testc')
const testP = path.join(destP, 'build')

// TODO: find how to wait for the archive to finish before running the tests, until then tests are not accurate ...

describe('createArchive function', () => {
  it('should  be a function', () => {
    expect(typeof zipCmd.cmd).toBe('function')
  })
})
