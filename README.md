# Clockwork cli (clkwrk)

cli to help project scaffolding, day to day task and other tools under a unified tool. in it current state it is configured with a very specific usecase (manipulate [adapt-framework](1) courses data), but it will have extensibily at the core

## usage
it is not published yet so if you want to test it (at your own risks)

- clone it
- install the needed dependecy
- link it (```npm link```, ```yarn link```, ...)

you can now use it ```clkwrk cmd [--args]```

### available cmds
#### ADAPT
these commands are specific for the adapt framework
##### add page level progress object
add or update to the given colletion the plp object with ```_isEnabled: true```
you can filter _components that won't have it 

## dev
### TODO 
***DOCUMENTATION :)***  

for the moment, read the code, only one cmd is implemented

- ```cli.js``` is the entry point
- ```tools/cmdParser.js``` parse commands and args and return a cmd array
- ```tools/adaptTools.js``` contain helpers for the adapt-framework updatePlp is used in the cli already

[1]:https://github.com/adaptlearning/adapt_framework