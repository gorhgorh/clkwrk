#!/usr/bin/env node
const debug = require('debug')('clkwrk:cmdParser')
const minimist = require('minimist')

// var args = parser.parseArgs();
debug('cli:cmdParser:init')

const demoCmd = {
  name: 'yo',
  alias: 'y',
  needVal: true,
  valueValidation: {
    type: 'oneOf',
    valVals: ['component', 'blocs', 'article', 'pth']
  }
}

const addPlp = {
  name: 'addPlp',
  alias: 'plp',
  needVal: true
}

let cmds = [demoCmd, addPlp]

function cmdParser (args = process.argv, addCmds) {
  if (addCmds && Array.isArray(addCmds)) {
    cmds = [...cmds, ...addCmds]
  }
  const opts = minimist(args)
  const currentDir = process.cwd()
  const rArgs = opts._
  const nodePth = rArgs.shift()
  const cliPath = rArgs.shift()
  const cwd = process.cwd()
  const parsed = { nodePth, cwd, ...opts, cliPath, currentDir }
  return parsed
}

function cmdSelector (parsed) {
  const cmdArr = []
  // const keys = Object.keys(parsed)
  parsed._.forEach((pCmd, i) => {
    cmds.forEach((cmd) => {
      if (cmd.name === pCmd || (cmd.alias && cmd.alias === pCmd)) {
        debug('cmd found:', cmd.name)
        const parsedCmd = {}
        parsedCmd.name = cmd.name
        if (cmd.needVal) {
          if (!parsed._[i + 1]) {
            parsedCmd.isInvalid = true
            parsedCmd.error = `missing argument after ${cmd.name}`
          } else {
            const spliced = parsed._.splice(i + 1, 1)
            parsedCmd.val = spliced[0]
          }
        }
        debug(parsedCmd)
        cmdArr.push(parsedCmd)
      }
    })
  })
  // console.log(parsed)
  return cmdArr.length > 0 ? cmdArr : false
}
// module.exports = cmdParser
module.exports = {
  cmdParser,
  cmdSelector
}
