const args = process.argv
const pathArgs = args.splice(0, 2)
const parsers = require('./cmdParser')
const cmdParser = parsers.cmdParser
const cmdSelector = parsers.cmdSelector
const fixArgs = [
  '-x',
  '3',
  '-y',
  '4',
  '-n5',
  '-abc',
  '--beep=boop',
  'foo',
  'bar',
  'baz'
]
const fakeArgs = [...pathArgs, ...fixArgs]

const fakeYo = [...pathArgs, 'yo']

const fixedParsed = cmdParser(fakeArgs)

test('should parse the fixed array', () => {
  const parsed = fixedParsed
  expect(parsed.x).toBe(3)
  expect(parsed.y).toBe(4)
  expect(parsed.x).toBe(3)
})

test('it should return a cmdError if a cmd that need a value is passed without it', () => {
  const parsedYoInvalid = cmdParser(fakeYo)
  const pCmds = cmdSelector(parsedYoInvalid)
  console.log(pCmds)
  expect(pCmds[0].isInvalid).toBeTruthy()
})

test('it should return valid command if arguments are met', () => {
  const last = '22'
  const parsedYo = cmdParser([...fakeYo, last])
  const pCmds = cmdSelector(parsedYo)
  console.log(pCmds)
  expect(pCmds[0].isInvalid).toBeUndefined()
})
