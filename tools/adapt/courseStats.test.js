const path = require('path')
const courseStats = require('./courseStats')

const cPath = path.join(__dirname, '../../fixtures/dummyCourseSimple/src/course/en')
describe('courseStats function', () => {
  it('should  be a function', async () => {
    expect(typeof courseStats).toBe('function')
    const stats = await courseStats(cPath)
    expect(stats).toBeTruthy()
  })
})
