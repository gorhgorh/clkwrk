const cmdName = 'zip'
// const cmdMsg = 'zip dirs'
const debug = require('debug')('clkwrk:' + cmdName)
const fs = require('fs')
const archiver = require('archiver')
const path = require('path')

function createArchive (srcPth, destPth, archName, cb) {
  console.log(archName, srcPth)

  // check if path exists

  // const buildPath = path.join(srcPth, 'build/')

  return new Promise((resolve, reject) => {
    const output = fs.createWriteStream(path.join(destPth, `${archName}.zip`))
    const archive = archiver('zip', {
      zlib: { level: 9 } // Sets the compression level.
    })
    output.on('open', () => {
    // listen for all archive data to be written
      // 'close' event is fired only when a file descriptor is involved
      output.on('close', function () {
        debug(archive.pointer() + ' total bytes')
        debug('archiver has been finalized and the output file descriptor has closed.')
        if (cb) cb()
        resolve(archive)
      })

      archive.on('warning', function (err) {
        if (err.code === 'ENOENT') {
          // log warning
          console.log('path not found', err)
        } else {
          // throw error
          debug(err)
          reject(err)
        }
      })

      archive.on('error', function (err) {
        reject(err)
      })

      // add files to the archives
      archive.bulk([
        { expand: true, cwd: srcPth, src: ['**'] }
      ])
      archive.finalize()
      archive.pipe(output)
    })
  })
  // return true
}

module.exports = createArchive
