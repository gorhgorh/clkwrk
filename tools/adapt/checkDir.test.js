const checkDir = require('./checkDir')
const path = require('path')

// const cPath = path.join(__dirname, '../../fixtures/testc')

const dPath = path.join(__dirname, '../../fixtures/dummyCourseMultilang')

describe('checkDir function', () => {
  it('should  be a function', () => {
    expect(typeof checkDir).toBe('function')
  })
  // it('it should return false if the path does not exists', async () => {
  //   const checked = await checkDir('psojiozje')
  //   expect(checked).toBe(false)
  // })
  // it('it should return an object with the path as "root" key if the path exists', async () => {
  //   const checked = await checkDir(cPath)
  //   expect(checked.root).toBe(cPath)
  // })
  it('it should return an object with the path as "root" key if the path exists', async () => {
    const checked = await checkDir(dPath)
    expect(checked.courseRoot).toBe(dPath)
  })
})
