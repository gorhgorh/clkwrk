const fs = require('fs-extra')
const _ = require('lodash')
const debug = require('debug')('clkwrk:checkDir')
const path = require('path')

const courseStats = require('./courseStats')
const courseReg = /course/

async function checkDir (pth) {
  const adaptDir = {
    isValid: false,
    hasCourseFolder: false,
    hasData: false,
    hasAdaptJson: false
  }
  try {
    // check if the path exists, and is a directory
    const rDir = await fs.promises.stat(pth)
    if (rDir.isDirectory() === false) return false
    const srcPath = path.join(pth, 'src')

    // get fw version from pkg
    try {
      const pkg = await fs.readJSON(path.join(pth, 'package.json'))
      adaptDir.hasPkg = true
      adaptDir.adaptVersion = pkg.version
    } catch (error) {
      // debug(error)
      adaptDir.hasPkg = false
    }

    // get adapt.json
    try {
      const adaptJson = await fs.readJSON(path.join(pth, 'adapt.json'))
      adaptDir.hasAdaptJson = true
      adaptDir.adaptJson = adaptJson
    } catch (error) {
      // debug(error)
      adaptDir.hasAdaptJson = false
    }

    // check that there is a 'src' dir
    try {
      const hasSrc = await fs.promises.stat(srcPath)
      adaptDir.courseRoot = pth
      adaptDir.hasSrc = hasSrc.isDirectory()
    } catch (error) {
      adaptDir.hasSrc = false
    }
    if (adaptDir.hasSrc === true) {
      const dir = await fs.promises.opendir(srcPath)
      const courses = []
      for await (const dirent of dir) {
        // check if curr dirent is a course folder
        if (courseReg.test(dirent.name) === true) {
          if (dirent.name === 'course') {
            adaptDir.hasSymlink = dirent.isSymbolicLink()
            adaptDir.hasCourseFolder = true
          }
          courses.push(dirent.name)
        }
      }

      // check for multicourse
      if (courses.length > 0) {
        adaptDir.courses = courses
        adaptDir.isMulticourse = courses.length > 1
      }

      if (adaptDir.hasCourseFolder) {
        // check for languages
        const langs = []
        const cDir = await fs.promises.opendir(path.join(srcPath, 'course'))
        for await (const dirent of cDir) {
          if (dirent.isDirectory()) {
            langs.push(dirent.name)
          } else if (dirent.name === 'config.json') {
            adaptDir.hasConfig = true
          }
        }
        adaptDir.langs = langs.length > 0 ? langs : false
        // adaptDir.hasData = adaptDir.langs !== false
      }
      // check the if the config exists and extract some data if it does
      if (adaptDir.hasConfig) {
        try {
          const cfgPath = path.join(srcPath, 'course/config.json')
          const cfg = await fs.readJSON(cfgPath)
          const defaultLang = cfg._defaultLanguage
          let hasDefLangDir = false
          adaptDir.langs.forEach(lang => {
            if (lang === defaultLang) hasDefLangDir = true
          })
          adaptDir.hasLangDir = hasDefLangDir
          adaptDir.defaultLang = defaultLang
          adaptDir.hasConfig = true
          _.set(adaptDir, 'adptJsons.cfgPath', cfgPath)
          // debug(cfg)
          // get the co/a/b/c json files

          if (hasDefLangDir === true) {
            const coursePath = path.join(srcPath, `course/${adaptDir.defaultLang}`)
            const cStats = await courseStats(coursePath)
            adaptDir.courseStats = cStats
          }
        } catch (error) {
          adaptDir.hasConfig = false
        }
      }
    }
    // check for courses

    // const landDirs = await fs.promises.opendir(srcPath)

    // debug(adaptDir)
    // fs.writeJSONSync(path.join(__dirname, '../../fixtures/wrt/sampleAdaptDir.json'), adaptDir, {spaces: 2})
    return adaptDir
  } catch (error) {
    console.info('error reading dir', pth)
    if (error.code === 'ENOENT') return false
    debug(error)
    return false
  }
}

module.exports = checkDir
