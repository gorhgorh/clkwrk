const debug = require('debug')('clkwrk:courseStat')
const fs = require('fs-extra')
const path = require('path')
const _ = require('lodash')
const files = ['course.json', 'contentObjects.json', 'articles.json', 'blocks.json', 'components.json']
async function courseStats (cPath) {
  const cStats = {}
  debug('starting coursestat', cPath)
  for await (const file of files) {
    const name = file.substring(0, file.length - 5)
    const pth = path.join(cPath, file)
    // console.log(file, name, pth)
    try {
      const data = await fs.readJSON(pth)
      let compList = []
      switch (name) {
        case 'course':
          cStats[name] = { data, pth }
          break
        case 'components':
          compList = _.uniq(data.map(comp => {
            return comp._component
          }))
          cStats[name] = { length: data.length, compList, pth }
          break
        default:
          // debug(name)
          cStats[name] = { length: data.length, pth }
          break
      }
    } catch (error) {
      cStats[name] = false
    }
  }
  // debug(cStats)
  return cStats
}

// str.substring(0, str.length - 1)

module.exports = courseStats
