const debug = require('debug')('clkwrk:displayStats')
const chalk = require('chalk')
const Table = require('tty-table')
const { green, cyan } = chalk
const adpArr = ['contentObjects', 'articles', 'blocks', 'components']

const twoCHeader = [
  {
    value: 'key',
    color: 'cyan'
  },
  {
    value: 'val',
    color: 'green',
    width: 10
    // formatter: function (value) {
    //   var str = `$${value.toFixed(2)}`
    //   if (value > 138) {
    //     str = chalk.bold.green(str)
    //   }
    //   return str
    // }
  }
]

function displayStats (adptSts) {
  // debug(adptSts)
  const { course, contentObjects, articles, blocks, components } = adptSts.courseStats
  const { courseRoot, defaultLang } = adptSts
  // debug(components)
  console.log(green(`Adapt course stats: ${courseRoot}`))
  console.log(green('config:'))
  console.log(green(`default lang: ${defaultLang}`))

  // if (course) {
  //   // console.log(course)
  //   const courseData = [
  //     ['_id', course.data._id]
  //   ]
  //   console.log(Table(twoCHeader, courseData).render())
  // }
  if (contentObjects && articles && blocks && components) {
    console.log('number of items')
    const courseData = [
      ['content objects', contentObjects.length],
      ['articles', articles.length],
      ['blocks', blocks.length],
      ['components', components.length]
    ]
    console.log(Table(twoCHeader, courseData).render())

    console.log(green('components type in course:'))
    components.compList.forEach(element => {
      console.log(`  - ${element}`)
    })
  }
  return true
}

module.exports = displayStats
