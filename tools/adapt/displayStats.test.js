const displayStats = require('./displayStats')
const fs = require('fs-extra')
const path = require('path')

const statData = fs.readJSONSync(path.join(__dirname, '../../fixtures/wrt/sampleAdaptDir.json'))

describe('displayStats function', () => {
  const displayed = displayStats(statData)
  it('should  be a function', () => {
    expect(typeof displayStats).toBe('function')
  })
  it('should retrun true', () => {
    expect(displayed).toBeTruthy()
  })
})
