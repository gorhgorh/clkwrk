const fs = require('fs-extra')
const debug = require('debug')('clkwrk:test:createArchive')
const createArchive = require('./createArchive')
const path = require('path')
const destP = path.join(__dirname, '../../fixtures/testc')
const testP = path.join(destP, 'build')

const testArchName = 'archTest'

const destArchP = path.join(destP, `${testArchName}.zip`)

// TODO: find how to wait for the archive to finish before running the tests, until then tests are not accurate ...
beforeAll(() => {
  fs.removeSync(destArchP)
  debug('removed previous archive')
})

describe('createArchive function', () => {
  it('should  be a function', async () => {
    const archive = await createArchive(testP, destP, testArchName)
    // console.log(archive)
    expect(typeof createArchive).toBe('function')
  })

  it('should have created a zip file', async () => {

    let hasFile = false

    try {
      await fs.promises.access(destArchP)
      hasFile = true
    } catch (error) {
      debug(error)
      hasFile = false
    }
    expect(hasFile).toBeTruthy()
  })
})
