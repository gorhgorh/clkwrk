const fs = require('fs-extra')
// const path = require('path')
const debug = require('debug')('clkwrk:adptTools')
const _ = require('lodash')
// const test =

/**
 * read and parse the json found at the provided path, or return false
 *
 * @param {string} pth path to the json
 * @returns {[]|false|{}} return the parsed json or false
 */
function readJson (pth) {
  try {
    const file = fs.readJSONSync(pth)
    // debug(file)
    return file
  } catch (error) {
    debug('file not found')
    return false
  }
}

/**
 * write a json file at the given path with the strigivied given data
 *
 * @param {string} pth path to the file to write the json
 * @param {[] | {}} data the data to write
 * @returns {boolean} write success
 */
function wrtJson (pth, data) {
  try {
    fs.writeJSONSync(pth, data, { spaces: 2 })
    debug('file written')
    return true
  } catch (error) {
    debug('error writing file:', pth)
    return false
  }
}

/**
 * add the plp property to an adapt object (co/a/b/c)
 * if the object already have a plp property it only set it to true
 *
 * @param {[]} data the collection of adapt objects
 * @param {string} filtered _component type that won't be updated
 * @returns {[] | false}
 */
function addPlp (data, filtered) {
  if (Array.isArray(data) === false) return false
  const plped = data.map(item => {
    const isEnabled = filtered !== item._component
    if (item._pageLevelProgress) {
      debug('already a plp key, updating')
      _.set(item, '_pageLevelProgress._isEnabled', isEnabled)
    } else {
      item._pageLevelProgress = {
        _isEnabled: isEnabled
      }
    }
    return item
  })
  return plped
}

/**
 * read an adapt json file and add the plp extention to all items, if filter is provided
 * the component that match the filter will be set to false
 *
 * @param {string} pth
 * @param {string} filtered
 * @returns {boolean}
 */
function updatePlp (pth, filtered) {
  const data = readJson(pth)
  // debug(data)
  if (Array.isArray(data) === false) {
    debug('data is not an array')
    return false
  }
  const plped = addPlp(data, filtered)
  if (plped === false) {
    debug('error while updating json')
  } else {
    wrtJson(pth, plped)
    return true
  }
}

function checkAdaptDir (path) {

}

function addProperty (col, prop, val) {
  if (!Array.isArray(col)) return false
  return col.map(item => {
    _.set(item, prop, val)
    return item
  })
}

module.exports = {
  readJson,
  wrtJson,
  updatePlp,
  addPlp,
  addProperty,
  checkAdaptDir
}
