const path = require('path')
const adaptTools = require('./adaptTools')
// const fs = require('fs-extra')
const _ = require('lodash')
const compPath = path.join(__dirname, '../fixtures/dummyCourseSimple/src/course/en/components.json')
// const wrtPath = path.join(__dirname, '../fixtures/wrt/testWrt.json')

function mkDummyJson () {
  return {
    _id: 'test-id',
    _component: 'text',
    title: 'test title',
    body: 'test body'
  }
}

function mkPlped () {
  const plped = mkDummyJson()
  plped._pageLevelProgress = {
    _isEnabled: false,
    _isShownInNavigationBar: true
  }
  return plped
}

const mediaComp = mkDummyJson()
mediaComp._component = 'media'

const {
  readJson,
  // wrtJson,
  addPlp,
  addProperty
} = adaptTools

const jsonData = readJson(compPath)

describe('read / write jsons', () => {
  it('it should return an array given a json array', () => {
    expect(Array.isArray(jsonData)).toBeTruthy()
  })
  it('id of the test json fist item is c-court-01', () => {
    expect(jsonData[0]._id).toBe('c-court-01')
  })
})

// describe('it should write json', () => {
//   try {
//     fs.removeSync(wrtPath)
//   } catch (error) {
//     console.log('no file to delete')
//   }
//   wrtJson(wrtPath, mkDummyJson())
// })

describe('plp', () => {
  const plped = addPlp([mediaComp])
  const compoO = plped[0]
  const wthPlp = addPlp([mkPlped()])
  it('there should be a _isEnabled key', () => {
    expect(compoO._pageLevelProgress._isEnabled).toBeTruthy()
  })
  describe('update existing plp Object', () => {
    it('there should be a _isEnabled key', () => {
      // const plpComp = mkPlped()
      // console.log(plpComp)
      expect(_.has(wthPlp[0], '_pageLevelProgress._isEnabled')).toBeTruthy()
    })
    it('should have been updated', () => {
      expect(wthPlp[0]._pageLevelProgress._isEnabled).toBeTruthy()
    })
    it('should have left the already set key as is', () => {
      expect(wthPlp[0]._pageLevelProgress._isShownInNavigationBar).toBeTruthy()
    })
  })
})

describe('add property', () => {
  const empty = [{}, { _test: { yarr: true } }]
  const added = addProperty(empty, '_test.yo', true)
  // console.log(added)
  it.each([[added[0], true], [added[1], true]])('test addProperty(%o, %s)',
    (input, expected) => {
      expect(_.has(input, '_test.yo')).toBe(expected)
    }
  )
  it('should only update key not replacing them', () => {
    expect(_.has(added[1], '_test.yarr')).toBeTruthy()
  })
})
